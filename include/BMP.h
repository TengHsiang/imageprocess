#define UINT8   unsigned char
#define UINT16  unsigned short
#define UINT32  unsigned int
#define INT8    char
#define INT16   short
#define INT32   int
#define ABS(x) ((x)<0?-(x):(x))
using namespace std;

const double RGB2YUVCoef_709[3][3] = { {0.2126,0.7152,0.0722}, {-0.09991,-0.33609,0.436}, {0.615,-0.55861,-0.05639} };
const double YUV2RGBCoef_709[3][3] = { {1,0,1.28033}, {1,-0.21482,-0.38059}, {1,2.12798,0} };
const double RGB2YUVCoef_601[3][3] = { {0.299,0.587,0.114}, {-0.14713,-0.28886,0.436}, {0.615,-0.51499,-0.10001} };
const double YUV2RGBCoef_601[3][3] = { {1, 0, 1.13983}, {1,-0.39465,-0.58060}, {1,2.03211,0} };

class BMP
{
  // MEMBER VARIABLE
  public :
    #ifndef _INC_BMP_TYPEDEF_H
      #define _INC_BMP_TYPEDEF_H "BMP_TypeDef.h"
      #include _INC_BMP_TYPEDEF_H
    #endif
    // STANDARD SEL
    static const unsigned int BT601 = 0;
    static const unsigned int BT709 = 1;
    // OPERATE VECTOR SEL
    static const unsigned int VECTOR_RGB = 0;
    static const unsigned int VECTOR_YUV = 1;
    // DIMENSION SEL
    static const unsigned int DIR_H = 0;
    static const unsigned int DIR_V = 1;
    
  private :  
    BMP_HEADER* Header    = NULL;
    PIXEL**     RGBVector = NULL;
    YUV_PIXEL** YUVVector = NULL;
    HSI_PIXEL** HSIVector = NULL;
    HISTOGRAM*  Histogram = NULL;
  
  // MEMBER FUNCTION
  public :
    BMP();
    BMP(string);
    ~BMP();
    bool ReadBMPFile();
    bool ReadBMPFile(string);
    bool WriteBMPFile();
    bool WriteBMPFile(string);
    bool SetPixel(const unsigned int, const unsigned int, PIXEL&);
    bool GenYHistogram(const unsigned int, const unsigned int, const unsigned int, const unsigned int);
    bool GetPixel(const unsigned int, const unsigned int, PIXEL&);
    bool GetPixel(const unsigned int, const unsigned int, YUV_PIXEL&);
    bool GetPixel(const unsigned int, const unsigned int, HSI_PIXEL&);
    bool RGB2YUV_OverWrite(const unsigned int);
    bool YUV2RGB_OverWrite(const unsigned int);
    bool RGB2HSI_OverWrite();
    bool HSI2RGB_OverWrite();
    bool CutHorizontalEdge(const unsigned int, const unsigned int);
    bool CutVerticalEdge(const unsigned int, const unsigned int);
    UINT32 GetYHistogram(const unsigned int);
    UINT32 GetWidth();
    UINT32 GetHeight();
    bool SetWidth(const unsigned int);
    bool SetHeight(const unsigned int);
    BMP_HEADER GetHeader();
    void PrintInfo2Stream(ostream&);
    bool Filter(const unsigned int*, const unsigned int, const unsigned int, const unsigned int);
    void DuplicateField(bool);
    void UpsideDown();
    void Mirror();
    void CutByRegion(unsigned int , unsigned int, unsigned int, unsigned int);
    void CutByIdx(unsigned int , unsigned int, unsigned int, unsigned int);
    void ScaleUp_Duplicate(unsigned int);
    
  private :
    bool ReadBMPData(string);
    bool WriteBMPData(string);
    void DeleteUserData();
    void ResetUserData();
    void Put1Byte(ofstream&, UINT8);
    void Put2Byte(ofstream&, UINT16);
    void Put3Byte(ofstream&, UINT32);
    void Put4Byte(ofstream&, UINT32);
};
