#ifndef _INC_BMP_TYPEDEF_H
  #define _INC_BMP_TYPEDEF_H "BMP_TypeDef.h"
  #include _INC_BMP_TYPEDEF_H
#endif

ostream& operator<<(ostream& os, const BMP::PIXEL& dat)
{
  os << "R:[" << dat.R << "] G:[" << dat.G << "] B:[" << dat.B << "]";
  return os;
}

ostream& operator<<(ostream& os, const BMP::YUV_PIXEL& dat)
{
  os << "Y:[" << dat.Y << "] U:[" << dat.U << "] V:[" << dat.V << "]";
  return os;
}

ostream& operator<<(ostream& os, const BMP::HSI_PIXEL& dat)
{
  os << "H:[" << dat.H << "] S:[" << dat.S << "] I:[" << dat.I << "]";
  return os;
}

ostream& operator<<(ostream& os, const BMP::INFO_HEADER& dat)
{
  os << "=====[ INFO HEADER ]=====" << endl;
  os << "BitmapHeaderSize = " <<      (unsigned int)dat.BitmapHeaderSize << endl;
  os << "Width = " <<                 (unsigned int)dat.Width << endl;
  os << "Height = " <<                (int)         dat.Height << endl;
  os << "Planes = " <<                (unsigned int)dat.Planes << endl;
  os << "BitPerPixel = " <<           (unsigned int)dat.BitPerPixel << endl;
  os << "Compression = " <<           (unsigned int)dat.Compression << endl;
  os << "BitmapDataSize = " <<        (unsigned int)dat.BitmapDataSize << endl;
  os << "HorizontalResolution = " <<  (unsigned int)dat.HorizontalResolution << endl;
  os << "VerticalResolution = " <<    (unsigned int)dat.VerticalResolution << endl;
  os << "UsedColors = " <<            (unsigned int)dat.UsedColors << endl;
  os << "ImportanColors = " <<        (unsigned int)dat.ImportanColors;
  return os;
}

ostream& operator<<(ostream& os, const BMP::FILE_HEADER& dat)
{
  os << "=====[ FILE HEADER ]=====" << endl;
  os << "Identifier = " <<        (unsigned int)dat.Identifier << endl;
  os << "FileSize = " <<          (unsigned int)dat.FileSize << endl;
  os << "Reserved = " <<          (unsigned int)dat.Reserved << endl;
  os << "BitmapDataOffset = " <<  (unsigned int)dat.BitmapDataOffset;
  return os;
}

ostream& operator<<(ostream& os, const BMP::BMP_HEADER& dat)
{
  os << (*dat.FileHeader) << endl << (*dat.InfoHeader);
  return os;
}

ostream& operator<<(ostream& os, BMP::BMP_HEADER& dat)
{
  os << *(dat.FileHeader) << endl << *(dat.InfoHeader);
  return os;
}

ostream& operator<<(ostream& os, BMP& dat)
{
  dat.PrintInfo2Stream(os);
  return os;
}

BMP::FILE_HEADER& BMP::FILE_HEADER::operator=( const FILE_HEADER& rhs )
{
  this->Identifier = rhs.BitmapDataOffset;
  this->FileSize = rhs.FileSize;
  this->Reserved = rhs.Reserved;
  this->BitmapDataOffset = rhs.BitmapDataOffset;
  return *this;
}

BMP::INFO_HEADER& BMP::INFO_HEADER::operator=( const INFO_HEADER& rhs)
{
  this->BitmapHeaderSize = rhs.BitmapHeaderSize;
  this->Width = rhs.Width;
  this->Height = rhs.Height;
  this->Planes = rhs.Planes;
  this->BitPerPixel = rhs.BitPerPixel;
  this->Compression = rhs.Compression;
  this->BitmapDataSize = rhs.BitmapDataSize;
  this->HorizontalResolution = rhs.HorizontalResolution;
  this->VerticalResolution = rhs.VerticalResolution;
  this->UsedColors = rhs.UsedColors;
  this->ImportanColors = rhs.ImportanColors;
  return *this;
}

BMP::BMP_HEADER& BMP::BMP_HEADER::operator=( const BMP_HEADER& rhs)
{
  *(this->FileHeader) = *(rhs.FileHeader);
  *(this->InfoHeader) = *(rhs.InfoHeader);
  return *this;
}

BMP::PIXEL& BMP::PIXEL::operator=( const BMP::PIXEL& rhs )
{
  this->R = rhs.R;
  this->G = rhs.G;
  this->B = rhs.B;
  this->Res = rhs.Res;
  return *this;
}
