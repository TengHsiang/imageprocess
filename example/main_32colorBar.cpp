#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string.h>
#include "../include/BMP.cpp"

using namespace std;

int main() {
	
	BMP outputBmp;
	BMP::PIXEL outputPixel;
	int i, j;
	
    outputBmp.SetWidth(3840);		    
    outputBmp.SetHeight(2160);
    
    for(i=0;i<2160;i++){
    for(j=0;j<3840;j++){
        int value = 8*(j/120);
        if(value>255) value = 255;
        
        if(i<30 || i>2130) {
            outputPixel.R=0;
            outputPixel.G=0;
            outputPixel.B=0;
        }else if(i>=30 && i<330){
            outputPixel.R=value;
            outputPixel.G=0;
            outputPixel.B=0;
        }else if(i>=330 && i<630){
            outputPixel.R=0;
            outputPixel.G=value;
            outputPixel.B=0;
        }else if(i>=630 && i<930){
            outputPixel.R=0;
            outputPixel.G=0;
            outputPixel.B=value;
        }else if(i>=930 && i<1230){
            outputPixel.R=0;
            outputPixel.G=value;
            outputPixel.B=value;
        }else if(i>=1230 && i<1530){
            outputPixel.R=value;
            outputPixel.G=0;
            outputPixel.B=value;
        }else if(i>=1530 && i<1830){
            outputPixel.R=value;
            outputPixel.G=value;
            outputPixel.B=0;
        }else {
            outputPixel.R=value;
            outputPixel.G=value;
            outputPixel.B=value;
        }           
        
        outputBmp.SetPixel(j, i, outputPixel);
    }}
    
    outputBmp.WriteBMPFile("32colorBar.bmp");
    return 0;
}

